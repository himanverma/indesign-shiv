import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  LOGIN_LOGIN_NOW_BEGIN,
  LOGIN_LOGIN_NOW_SUCCESS,
  LOGIN_LOGIN_NOW_FAILURE,
  LOGIN_LOGIN_NOW_DISMISS_ERROR,
} from '../../../../src/features/login/redux/constants';

import {
  loginNow,
  dismissLoginNowError,
  reducer,
} from '../../../../src/features/login/redux/loginNow';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('login/redux/loginNow', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when loginNow succeeds', () => {
    const store = mockStore({});

    return store.dispatch(loginNow())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', LOGIN_LOGIN_NOW_BEGIN);
        expect(actions[1]).toHaveProperty('type', LOGIN_LOGIN_NOW_SUCCESS);
      });
  });

  it('dispatches failure action when loginNow fails', () => {
    const store = mockStore({});

    return store.dispatch(loginNow({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', LOGIN_LOGIN_NOW_BEGIN);
        expect(actions[1]).toHaveProperty('type', LOGIN_LOGIN_NOW_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissLoginNowError', () => {
    const expectedAction = {
      type: LOGIN_LOGIN_NOW_DISMISS_ERROR,
    };
    expect(dismissLoginNowError()).toEqual(expectedAction);
  });

  it('handles action type LOGIN_LOGIN_NOW_BEGIN correctly', () => {
    const prevState = { loginNowPending: false };
    const state = reducer(
      prevState,
      { type: LOGIN_LOGIN_NOW_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.loginNowPending).toBe(true);
  });

  it('handles action type LOGIN_LOGIN_NOW_SUCCESS correctly', () => {
    const prevState = { loginNowPending: true };
    const state = reducer(
      prevState,
      { type: LOGIN_LOGIN_NOW_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.loginNowPending).toBe(false);
  });

  it('handles action type LOGIN_LOGIN_NOW_FAILURE correctly', () => {
    const prevState = { loginNowPending: true };
    const state = reducer(
      prevState,
      { type: LOGIN_LOGIN_NOW_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.loginNowPending).toBe(false);
    expect(state.loginNowError).toEqual(expect.anything());
  });

  it('handles action type LOGIN_LOGIN_NOW_DISMISS_ERROR correctly', () => {
    const prevState = { loginNowError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: LOGIN_LOGIN_NOW_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.loginNowError).toBe(null);
  });
});

