import React from 'react';
import { shallow } from 'enzyme';
import { AddClients } from '../../../src/features/clients/AddClients';

describe('clients/AddClients', () => {
  it('renders node with correct class name', () => {
    const props = {
      clients: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <AddClients {...props} />
    );

    expect(
      renderedComponent.find('.clients-add-clients').length
    ).toBe(1);
  });
});
