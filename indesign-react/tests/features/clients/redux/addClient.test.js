import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  CLIENTS_ADD_CLIENT_BEGIN,
  CLIENTS_ADD_CLIENT_SUCCESS,
  CLIENTS_ADD_CLIENT_FAILURE,
  CLIENTS_ADD_CLIENT_DISMISS_ERROR,
} from '../../../../src/features/clients/redux/constants';

import {
  addClient,
  dismissAddClientError,
  reducer,
} from '../../../../src/features/clients/redux/addClient';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('clients/redux/addClient', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when addClient succeeds', () => {
    const store = mockStore({});

    return store.dispatch(addClient())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', CLIENTS_ADD_CLIENT_BEGIN);
        expect(actions[1]).toHaveProperty('type', CLIENTS_ADD_CLIENT_SUCCESS);
      });
  });

  it('dispatches failure action when addClient fails', () => {
    const store = mockStore({});

    return store.dispatch(addClient({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', CLIENTS_ADD_CLIENT_BEGIN);
        expect(actions[1]).toHaveProperty('type', CLIENTS_ADD_CLIENT_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissAddClientError', () => {
    const expectedAction = {
      type: CLIENTS_ADD_CLIENT_DISMISS_ERROR,
    };
    expect(dismissAddClientError()).toEqual(expectedAction);
  });

  it('handles action type CLIENTS_ADD_CLIENT_BEGIN correctly', () => {
    const prevState = { addClientPending: false };
    const state = reducer(
      prevState,
      { type: CLIENTS_ADD_CLIENT_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addClientPending).toBe(true);
  });

  it('handles action type CLIENTS_ADD_CLIENT_SUCCESS correctly', () => {
    const prevState = { addClientPending: true };
    const state = reducer(
      prevState,
      { type: CLIENTS_ADD_CLIENT_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addClientPending).toBe(false);
  });

  it('handles action type CLIENTS_ADD_CLIENT_FAILURE correctly', () => {
    const prevState = { addClientPending: true };
    const state = reducer(
      prevState,
      { type: CLIENTS_ADD_CLIENT_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addClientPending).toBe(false);
    expect(state.addClientError).toEqual(expect.anything());
  });

  it('handles action type CLIENTS_ADD_CLIENT_DISMISS_ERROR correctly', () => {
    const prevState = { addClientError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: CLIENTS_ADD_CLIENT_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addClientError).toBe(null);
  });
});

