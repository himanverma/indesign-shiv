import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  CLIENTS_LIST_CLIENTS_BEGIN,
  CLIENTS_LIST_CLIENTS_SUCCESS,
  CLIENTS_LIST_CLIENTS_FAILURE,
  CLIENTS_LIST_CLIENTS_DISMISS_ERROR,
} from '../../../../src/features/clients/redux/constants';

import {
  listClients,
  dismissListClientsError,
  reducer,
} from '../../../../src/features/clients/redux/listClients';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('clients/redux/listClients', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when listClients succeeds', () => {
    const store = mockStore({});

    return store.dispatch(listClients())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', CLIENTS_LIST_CLIENTS_BEGIN);
        expect(actions[1]).toHaveProperty('type', CLIENTS_LIST_CLIENTS_SUCCESS);
      });
  });

  it('dispatches failure action when listClients fails', () => {
    const store = mockStore({});

    return store.dispatch(listClients({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', CLIENTS_LIST_CLIENTS_BEGIN);
        expect(actions[1]).toHaveProperty('type', CLIENTS_LIST_CLIENTS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissListClientsError', () => {
    const expectedAction = {
      type: CLIENTS_LIST_CLIENTS_DISMISS_ERROR,
    };
    expect(dismissListClientsError()).toEqual(expectedAction);
  });

  it('handles action type CLIENTS_LIST_CLIENTS_BEGIN correctly', () => {
    const prevState = { listClientsPending: false };
    const state = reducer(
      prevState,
      { type: CLIENTS_LIST_CLIENTS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listClientsPending).toBe(true);
  });

  it('handles action type CLIENTS_LIST_CLIENTS_SUCCESS correctly', () => {
    const prevState = { listClientsPending: true };
    const state = reducer(
      prevState,
      { type: CLIENTS_LIST_CLIENTS_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listClientsPending).toBe(false);
  });

  it('handles action type CLIENTS_LIST_CLIENTS_FAILURE correctly', () => {
    const prevState = { listClientsPending: true };
    const state = reducer(
      prevState,
      { type: CLIENTS_LIST_CLIENTS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listClientsPending).toBe(false);
    expect(state.listClientsError).toEqual(expect.anything());
  });

  it('handles action type CLIENTS_LIST_CLIENTS_DISMISS_ERROR correctly', () => {
    const prevState = { listClientsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: CLIENTS_LIST_CLIENTS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listClientsError).toBe(null);
  });
});

