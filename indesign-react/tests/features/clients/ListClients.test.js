import React from 'react';
import { shallow } from 'enzyme';
import { ListClients } from '../../../src/features/clients/ListClients';

describe('clients/ListClients', () => {
  it('renders node with correct class name', () => {
    const props = {
      clients: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <ListClients {...props} />
    );

    expect(
      renderedComponent.find('.clients-list-clients').length
    ).toBe(1);
  });
});
