import React from 'react';
import { shallow } from 'enzyme';
import { AutoPaging } from '../../../src/features/documents/AutoPaging';

describe('documents/AutoPaging', () => {
  it('renders node with correct class name', () => {
    const props = {
      documents: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <AutoPaging {...props} />
    );

    expect(
      renderedComponent.find('.documents-auto-paging').length
    ).toBe(1);
  });
});
