import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  DOCUMENTS_UPLOAD_AUTO_PAGING_BEGIN,
  DOCUMENTS_UPLOAD_AUTO_PAGING_SUCCESS,
  DOCUMENTS_UPLOAD_AUTO_PAGING_FAILURE,
  DOCUMENTS_UPLOAD_AUTO_PAGING_DISMISS_ERROR,
} from '../../../../src/features/documents/redux/constants';

import {
  uploadAutoPaging,
  dismissUploadAutoPagingError,
  reducer,
} from '../../../../src/features/documents/redux/uploadAutoPaging';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('documents/redux/uploadAutoPaging', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when uploadAutoPaging succeeds', () => {
    const store = mockStore({});

    return store.dispatch(uploadAutoPaging())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', DOCUMENTS_UPLOAD_AUTO_PAGING_BEGIN);
        expect(actions[1]).toHaveProperty('type', DOCUMENTS_UPLOAD_AUTO_PAGING_SUCCESS);
      });
  });

  it('dispatches failure action when uploadAutoPaging fails', () => {
    const store = mockStore({});

    return store.dispatch(uploadAutoPaging({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', DOCUMENTS_UPLOAD_AUTO_PAGING_BEGIN);
        expect(actions[1]).toHaveProperty('type', DOCUMENTS_UPLOAD_AUTO_PAGING_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissUploadAutoPagingError', () => {
    const expectedAction = {
      type: DOCUMENTS_UPLOAD_AUTO_PAGING_DISMISS_ERROR,
    };
    expect(dismissUploadAutoPagingError()).toEqual(expectedAction);
  });

  it('handles action type DOCUMENTS_UPLOAD_AUTO_PAGING_BEGIN correctly', () => {
    const prevState = { uploadAutoPagingPending: false };
    const state = reducer(
      prevState,
      { type: DOCUMENTS_UPLOAD_AUTO_PAGING_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.uploadAutoPagingPending).toBe(true);
  });

  it('handles action type DOCUMENTS_UPLOAD_AUTO_PAGING_SUCCESS correctly', () => {
    const prevState = { uploadAutoPagingPending: true };
    const state = reducer(
      prevState,
      { type: DOCUMENTS_UPLOAD_AUTO_PAGING_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.uploadAutoPagingPending).toBe(false);
  });

  it('handles action type DOCUMENTS_UPLOAD_AUTO_PAGING_FAILURE correctly', () => {
    const prevState = { uploadAutoPagingPending: true };
    const state = reducer(
      prevState,
      { type: DOCUMENTS_UPLOAD_AUTO_PAGING_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.uploadAutoPagingPending).toBe(false);
    expect(state.uploadAutoPagingError).toEqual(expect.anything());
  });

  it('handles action type DOCUMENTS_UPLOAD_AUTO_PAGING_DISMISS_ERROR correctly', () => {
    const prevState = { uploadAutoPagingError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: DOCUMENTS_UPLOAD_AUTO_PAGING_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.uploadAutoPagingError).toBe(null);
  });
});

