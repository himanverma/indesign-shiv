import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  DOCUMENTS_LIST_AUTO_PAGING_BEGIN,
  DOCUMENTS_LIST_AUTO_PAGING_SUCCESS,
  DOCUMENTS_LIST_AUTO_PAGING_FAILURE,
  DOCUMENTS_LIST_AUTO_PAGING_DISMISS_ERROR,
} from '../../../../src/features/documents/redux/constants';

import {
  listAutoPaging,
  dismissListAutoPagingError,
  reducer,
} from '../../../../src/features/documents/redux/listAutoPaging';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('documents/redux/listAutoPaging', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when listAutoPaging succeeds', () => {
    const store = mockStore({});

    return store.dispatch(listAutoPaging())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', DOCUMENTS_LIST_AUTO_PAGING_BEGIN);
        expect(actions[1]).toHaveProperty('type', DOCUMENTS_LIST_AUTO_PAGING_SUCCESS);
      });
  });

  it('dispatches failure action when listAutoPaging fails', () => {
    const store = mockStore({});

    return store.dispatch(listAutoPaging({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', DOCUMENTS_LIST_AUTO_PAGING_BEGIN);
        expect(actions[1]).toHaveProperty('type', DOCUMENTS_LIST_AUTO_PAGING_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissListAutoPagingError', () => {
    const expectedAction = {
      type: DOCUMENTS_LIST_AUTO_PAGING_DISMISS_ERROR,
    };
    expect(dismissListAutoPagingError()).toEqual(expectedAction);
  });

  it('handles action type DOCUMENTS_LIST_AUTO_PAGING_BEGIN correctly', () => {
    const prevState = { listAutoPagingPending: false };
    const state = reducer(
      prevState,
      { type: DOCUMENTS_LIST_AUTO_PAGING_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listAutoPagingPending).toBe(true);
  });

  it('handles action type DOCUMENTS_LIST_AUTO_PAGING_SUCCESS correctly', () => {
    const prevState = { listAutoPagingPending: true };
    const state = reducer(
      prevState,
      { type: DOCUMENTS_LIST_AUTO_PAGING_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listAutoPagingPending).toBe(false);
  });

  it('handles action type DOCUMENTS_LIST_AUTO_PAGING_FAILURE correctly', () => {
    const prevState = { listAutoPagingPending: true };
    const state = reducer(
      prevState,
      { type: DOCUMENTS_LIST_AUTO_PAGING_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listAutoPagingPending).toBe(false);
    expect(state.listAutoPagingError).toEqual(expect.anything());
  });

  it('handles action type DOCUMENTS_LIST_AUTO_PAGING_DISMISS_ERROR correctly', () => {
    const prevState = { listAutoPagingError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: DOCUMENTS_LIST_AUTO_PAGING_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listAutoPagingError).toBe(null);
  });
});

