import React from 'react';
import { shallow } from 'enzyme';
import { AddUsers } from '../../../src/features/users/AddUsers';

describe('users/AddUsers', () => {
  it('renders node with correct class name', () => {
    const props = {
      users: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <AddUsers {...props} />
    );

    expect(
      renderedComponent.find('.users-add-users').length
    ).toBe(1);
  });
});
