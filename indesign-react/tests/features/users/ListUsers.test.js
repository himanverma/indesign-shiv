import React from 'react';
import { shallow } from 'enzyme';
import { ListUsers } from '../../../src/features/users/ListUsers';

describe('users/ListUsers', () => {
  it('renders node with correct class name', () => {
    const props = {
      users: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <ListUsers {...props} />
    );

    expect(
      renderedComponent.find('.users-list-users').length
    ).toBe(1);
  });
});
