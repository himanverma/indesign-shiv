import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  USERS_LIST_USERS_BEGIN,
  USERS_LIST_USERS_SUCCESS,
  USERS_LIST_USERS_FAILURE,
  USERS_LIST_USERS_DISMISS_ERROR,
} from '../../../../src/features/users/redux/constants';

import {
  listUsers,
  dismissListUsersError,
  reducer,
} from '../../../../src/features/users/redux/listUsers';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('users/redux/listUsers', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when listUsers succeeds', () => {
    const store = mockStore({});

    return store.dispatch(listUsers())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', USERS_LIST_USERS_BEGIN);
        expect(actions[1]).toHaveProperty('type', USERS_LIST_USERS_SUCCESS);
      });
  });

  it('dispatches failure action when listUsers fails', () => {
    const store = mockStore({});

    return store.dispatch(listUsers({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', USERS_LIST_USERS_BEGIN);
        expect(actions[1]).toHaveProperty('type', USERS_LIST_USERS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissListUsersError', () => {
    const expectedAction = {
      type: USERS_LIST_USERS_DISMISS_ERROR,
    };
    expect(dismissListUsersError()).toEqual(expectedAction);
  });

  it('handles action type USERS_LIST_USERS_BEGIN correctly', () => {
    const prevState = { listUsersPending: false };
    const state = reducer(
      prevState,
      { type: USERS_LIST_USERS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listUsersPending).toBe(true);
  });

  it('handles action type USERS_LIST_USERS_SUCCESS correctly', () => {
    const prevState = { listUsersPending: true };
    const state = reducer(
      prevState,
      { type: USERS_LIST_USERS_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listUsersPending).toBe(false);
  });

  it('handles action type USERS_LIST_USERS_FAILURE correctly', () => {
    const prevState = { listUsersPending: true };
    const state = reducer(
      prevState,
      { type: USERS_LIST_USERS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listUsersPending).toBe(false);
    expect(state.listUsersError).toEqual(expect.anything());
  });

  it('handles action type USERS_LIST_USERS_DISMISS_ERROR correctly', () => {
    const prevState = { listUsersError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: USERS_LIST_USERS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.listUsersError).toBe(null);
  });
});

