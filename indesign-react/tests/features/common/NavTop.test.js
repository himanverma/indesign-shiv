import React from 'react';
import { shallow } from 'enzyme';
import { NavTop } from '../../../src/features/common/NavTop';

describe('common/NavTop', () => {
  it('renders node with correct class name', () => {
    const props = {
      common: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <NavTop {...props} />
    );

    expect(
      renderedComponent.find('.common-nav-top').length
    ).toBe(1);
  });
});
