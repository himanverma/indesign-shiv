import React from 'react';
import { shallow } from 'enzyme';
import { Nav } from '../../../src/features/common/Nav';

describe('common/Nav', () => {
  it('renders node with correct class name', () => {
    const props = {
      common: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <Nav {...props} />
    );

    expect(
      renderedComponent.find('.common-nav').length
    ).toBe(1);
  });
});
