import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  COMMON_LOGOUT_NOW_BEGIN,
  COMMON_LOGOUT_NOW_SUCCESS,
  COMMON_LOGOUT_NOW_FAILURE,
  COMMON_LOGOUT_NOW_DISMISS_ERROR,
} from '../../../../src/features/common/redux/constants';

import {
  logoutNow,
  dismissLogoutNowError,
  reducer,
} from '../../../../src/features/common/redux/logoutNow';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('common/redux/logoutNow', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when logoutNow succeeds', () => {
    const store = mockStore({});

    return store.dispatch(logoutNow())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', COMMON_LOGOUT_NOW_BEGIN);
        expect(actions[1]).toHaveProperty('type', COMMON_LOGOUT_NOW_SUCCESS);
      });
  });

  it('dispatches failure action when logoutNow fails', () => {
    const store = mockStore({});

    return store.dispatch(logoutNow({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', COMMON_LOGOUT_NOW_BEGIN);
        expect(actions[1]).toHaveProperty('type', COMMON_LOGOUT_NOW_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissLogoutNowError', () => {
    const expectedAction = {
      type: COMMON_LOGOUT_NOW_DISMISS_ERROR,
    };
    expect(dismissLogoutNowError()).toEqual(expectedAction);
  });

  it('handles action type COMMON_LOGOUT_NOW_BEGIN correctly', () => {
    const prevState = { logoutNowPending: false };
    const state = reducer(
      prevState,
      { type: COMMON_LOGOUT_NOW_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.logoutNowPending).toBe(true);
  });

  it('handles action type COMMON_LOGOUT_NOW_SUCCESS correctly', () => {
    const prevState = { logoutNowPending: true };
    const state = reducer(
      prevState,
      { type: COMMON_LOGOUT_NOW_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.logoutNowPending).toBe(false);
  });

  it('handles action type COMMON_LOGOUT_NOW_FAILURE correctly', () => {
    const prevState = { logoutNowPending: true };
    const state = reducer(
      prevState,
      { type: COMMON_LOGOUT_NOW_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.logoutNowPending).toBe(false);
    expect(state.logoutNowError).toEqual(expect.anything());
  });

  it('handles action type COMMON_LOGOUT_NOW_DISMISS_ERROR correctly', () => {
    const prevState = { logoutNowError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: COMMON_LOGOUT_NOW_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.logoutNowError).toBe(null);
  });
});

