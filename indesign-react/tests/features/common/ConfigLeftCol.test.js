import React from 'react';
import { shallow } from 'enzyme';
import { ConfigLeftCol } from '../../../src/features/common/ConfigLeftCol';

describe('common/ConfigLeftCol', () => {
  it('renders node with correct class name', () => {
    const props = {
      common: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <ConfigLeftCol {...props} />
    );

    expect(
      renderedComponent.find('.common-config-left-col').length
    ).toBe(1);
  });
});
