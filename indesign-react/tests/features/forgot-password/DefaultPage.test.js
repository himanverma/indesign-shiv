import React from 'react';
import { shallow } from 'enzyme';
import { DefaultPage } from '../../../src/features/forgot-password/DefaultPage';

describe('forgot-password/DefaultPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      forgotPassword: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <DefaultPage {...props} />
    );

    expect(
      renderedComponent.find('.forgot-password-default-page').length
    ).toBe(1);
  });
});
