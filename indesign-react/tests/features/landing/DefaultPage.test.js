import React from 'react';
import { shallow } from 'enzyme';
import { DefaultPage } from '../../../src/features/landing/DefaultPage';

describe('landing/DefaultPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      landing: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <DefaultPage {...props} />
    );

    expect(
      renderedComponent.find('.landing-default-page').length
    ).toBe(1);
  });
});
