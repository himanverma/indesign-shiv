import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';

import Notifications, { notify } from 'react-notify-toast';
import { Formik } from 'formik'
import { Form, Button, Col, InputGroup } from 'react-bootstrap';
import Pace from 'react-pace-progress';
import Nav from '../common/Nav';
import NavTop from '../common/NavTop';
import ConfigLeftCol from '../common/ConfigLeftCol';
import $ from "jquery";
import * as yup from 'yup';

const schema = yup.object({
  company_name: yup.string().required(),
  admin_name: yup.string().required(),
  admin_email: yup.string().email().required(),
  ce: yup.bool().required(),
  pg: yup.bool().required(),
  xml: yup.bool().required(),
  epub: yup.bool().required(),
  country: yup.string().required(),
  password: yup.string().required(),
  phone: yup.string().required()
});

export class AddClients extends Component {
  static propTypes = {
    clients: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      isLoading: true
    };
    this.saveClient = this.saveClient.bind(this);
  }
  componentDidMount() {
    // $('body').addClass('gray-bg');
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 3000);
  }
  componentWillUnmount() {
    // $('body').removeClass('gray-bg');
  }

  saveClient(values, form) {
    this.props.actions.addClient(values).then((data) => {
      notify.show(data.data.company.company_name + " has been added as a new client.", 'success');
      setTimeout(() => {
        this.props.history.push('/dashboard');
      }, 2000);
      console.info(data);
    }).catch((err) => {
      console.log("Error:", err);
    });
  }

  renderForm() {
    return (
      <Formik
        validationSchema={schema}
        onSubmit={this.saveClient}
        initialValues={{
          company_name: '',
          admin_name: '',
          admin_email: '',
          ce: false,
          pg: true,
          xml: false,
          epub: false,
          country: '',
          password: '',
          phone: ''
        }}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          touched,
          isValid,
          errors,
        }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Row>
                <Form.Group as={Col} md="6" controlId="validationFormik01">
                  <Form.Label>Company Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="company_name"
                    value={values.company_name}
                    onChange={handleChange}
                    isValid={touched.company_name && !errors.company_name}
                  />
                  <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6" controlId="validationFormik02">
                  <Form.Label>Administrator Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="admin_name"
                    value={values.admin_name}
                    onChange={handleChange}
                    isValid={touched.admin_name && !errors.admin_name}
                  />
                  <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="4" controlId="validationFormikUsername">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Administrator's Email"
                    aria-describedby="inputGroupPrepend"
                    name="admin_email"
                    value={values.admin_email}
                    onChange={handleChange}
                    isInvalid={!!errors.admin_email}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.admin_email}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Label>Products </Form.Label>
                <Form.Group as={Col} md="6" controlId="validationFormik03">

                  <Form.Check
                    inline
                    name="ce"
                    label="CE"
                    onChange={handleChange}
                    isInvalid={!!errors.ce}
                    feedback={errors.ce}
                    id="validationFormik0"
                    value="ce"
                  />
                  <Form.Check
                    inline
                    name="pg"
                    label="PG"
                    onChange={handleChange}
                    isInvalid={!!errors.pg}
                    feedback={errors.pg}
                    id="validationFormik0"
                    value="pg"
                  />
                  <Form.Check
                    inline
                    name="xml"
                    label="XML"
                    onChange={handleChange}
                    isInvalid={!!errors.xml}
                    feedback={errors.xml}
                    id="validationFormik0"
                    value="xml"
                  />
                  <Form.Check
                    inline
                    name="epub"
                    label="ePub"
                    onChange={handleChange}
                    isInvalid={!!errors.epub}
                    feedback={errors.epub}
                    id="validationFormik0"
                    value="epub"
                  />
                  {/* <Form.Control.Feedback type="invalid">
                    {errors.city}
                  </Form.Control.Feedback> */}
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="3" controlId="validationFormik04">
                  <Form.Label>Country</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Country Name"
                    name="country"
                    value={values.country}
                    onChange={handleChange}
                    isInvalid={!!errors.country}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.country}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6" controlId="validationFormik05">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    name="password"
                    value={values.password}
                    onChange={handleChange}
                    isInvalid={!!errors.password}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.password}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6" controlId="validationFormik05">
                  <Form.Label>Phone</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Phone"
                    name="phone"
                    value={values.phone}
                    onChange={handleChange}
                    isInvalid={!!errors.phone}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.phone}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>

              <Button type="submit">Submit form</Button>
            </Form>
          )}
      </Formik>
    );
  }

  render() {
    return (
      <React.Fragment>
        {this.state.isLoading ? <Pace color="#27ae60" height={2} className="loader" /> : null}

        <div className="clients-add-clients" >
          <Notifications />
          <Nav />
          <div id="page-wrapper" className="gray-bg">
            <div className="row border-bottom">
              <NavTop history={this.props.history} />
            </div>
            <div className="wrapper wrapper-content animated fadeInRight">
              <div className="row">
                <ConfigLeftCol />
                <div className="col-md-10">
                  <div className="ibox">
                    <div className="ibox-title">
                      <h3> Add Client </h3>
                    </div>
                    <div className="ibox-content">
                    {this.renderForm()}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    clients: state.clients,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddClients);
