import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';


import Notifications, { notify } from 'react-notify-toast';
import Pace from 'react-pace-progress';
import Nav from '../common/Nav';
import NavTop from '../common/NavTop';
import $ from "jquery";
import  ConfigLeftCol from '../common/ConfigLeftCol';


export class ListClients extends Component {
  static propTypes = {
    clients: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  constructor() {
    super();
    this.state = {
      isLoading: true
    };
  }

  ClientsList() {
    return (<div className="ibox ">
      <div className="ibox-title">
        <h5>Clients List </h5>
        <div className="ibox-tools">
          <a className="collapse-link">
            <i className="fa fa-chevron-up" />
          </a>
          {/* <a className="close-link">
            <i className="fa fa-times" />
          </a> */}
        </div>
      </div>
      <div className="ibox-content">
        <table className="table table-bordered">
          <thead>
            <tr>
              <th rowSpan={2}>ID</th>
              <th rowSpan={2}>Company Name</th>
              <th rowSpan={2}>Admin Name</th>
              <th rowSpan={2}>Admin Email</th>
              <th colSpan={4}>Products</th>
              <th rowSpan={2}>Status</th>
            </tr>
            <tr>
              <th>CE</th>
              <th>PG</th>
              <th>XML</th>
              <th>ePUB</th>
            </tr>
          </thead>
          <tbody>
            {this.props.clientsList.map((data, i) => {
              return <tr key={i}>
                <td>{data.id}</td>
                <td>{data.Company.company_name}</td>
                <td>{data.name}</td>
                <td>{data.email}</td>
                <td>{data.Company.ce ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.Company.pg ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.Company.xml ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.Company.epub ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>Active</td>
              </tr>
            })}
          </tbody>

        </table>
      </div>
    </div>
    )
  }

  render() {
    return (
      <React.Fragment>
        {this.state.isLoading ? <Pace color="#27ae60" height={2} className="loader" /> : null}

        <div className="clients-list-clients" >
          <Notifications />
          <Nav />
          <div id="page-wrapper" className="gray-bg">
            <div className="row border-bottom">
              <NavTop history={this.props.history} />
            </div>
            <div className="wrapper wrapper-content animated fadeInRight">
              <div className="row">
                <ConfigLeftCol />
                <div className="col-md-10">
                  {this.ClientsList()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    clients: state.clients,
    clientsList: state.clients.clientsList,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListClients);
