// This is the JSON way to define React Router rules in a Rekit app.
// Learn more from: http://rekit.js.org/docs/routing.html

import {
  ListClients,
  AddClients,
} from './';

export default {
  path: 'clients',
  name: 'Clients',
  childRoutes: [
    { path: 'list', name: 'List Clients', component: ListClients, isIndex: true },
    { path: 'new', name: 'Add Clients', component: AddClients },
  ],
};
