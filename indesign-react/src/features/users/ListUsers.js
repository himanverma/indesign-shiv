import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';

import Notifications, { notify } from 'react-notify-toast';
import Pace from 'react-pace-progress';
import Nav from '../common/Nav';
import NavTop from '../common/NavTop';
import ConfigLeftCol from '../common/ConfigLeftCol';
import $ from "jquery";

import Switch from 'react-switchery';

export class ListUsers extends Component {
  static propTypes = {
    users: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      isLoading: true
    };
    this.toggleStatus = this.toggleStatus.bind(this);
  }
  componentDidMount() {
    this.props.actions.listUsers();
  }
  toggleStatus(val){
    console.log(val);
  }

  UsersList() {
    return (<div className="ibox ">
      {/* <div className="ibox-title">
        <h5>Users List </h5>
        <div className="ibox-tools">
          <a className="collapse-link">
            <i className="fa fa-chevron-up" />
          </a>
          <a className="close-link">
            <i className="fa fa-times" />
          </a> 
        </div>
      </div> */}
      <div className="ibox-content">
        <table className="table table-bordered">
          <thead>
            <tr>
              <th rowSpan={2}>ID</th>
              <th rowSpan={2}>Company Name</th>
              <th rowSpan={2}>Name</th>
              <th rowSpan={2}>Email</th>
              <th colSpan={4}>Products</th>
              <th rowSpan={2}>Status</th>
              <th rowSpan={2}>Actions</th>
            </tr>
            <tr>
              <th>CE</th>
              <th>PG</th>
              <th>XML</th>
              <th>ePUB</th>
            </tr>
          </thead>
          <tbody>
            {this.props.usersList.map((data, i) => {
              return <tr key={i}>
                <td>{data.id}</td>
                <td>{data.Company.company_name}</td>
                <td>{data.name}</td>
                <td>{data.email}</td>
                <td>{data.ce ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.pg ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.xml ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.epub ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>Active</td>
                <td>
                  <Switch
                    className="switch-class"
                    onChange={this.toggleStatus}
                    options={
                      {
                        color: '#474F79',
                        size: 'small'
                      }
                    }
                    checked={data.status == 1}
                  />

                </td>
              </tr>
            })}
          </tbody>

        </table>
      </div>
    </div>
    )
  }

  render() {

    return (
      <React.Fragment>
        {this.state.isLoading ? <Pace color="#27ae60" height={2} className="loader" /> : null}

        <div className="users-list-users" >
          <Notifications />
          <Nav />
          <div id="page-wrapper" className="gray-bg">
            <div className="row border-bottom">
              <NavTop history={this.props.history} />
            </div>
            <div className="wrapper wrapper-content animated fadeInRight">
              <div className="row">
                <ConfigLeftCol />
                <div className="col-md-10">
                  <div className="ibox">
                    <div className="ibox-title">
                      <h3>Manage Users</h3>
                    </div>
                    <div className="ibox-content">
                      {this.UsersList()}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    users: state.users,
    usersList: state.users.usersList
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListUsers);
