import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';


import Notifications, { notify } from 'react-notify-toast';
import { Formik } from 'formik'
import { Form, Button, Col, InputGroup } from 'react-bootstrap';
import Pace from 'react-pace-progress';
import Nav from '../common/Nav';
import NavTop from '../common/NavTop';
import ConfigLeftCol from '../common/ConfigLeftCol';
import $ from "jquery";
import * as yup from 'yup';

const schema = yup.object({
  first_name: yup.string().required(),
  user_email: yup.string().email().required(),
  password: yup.string().required(),
  // phone: yup.string().required()
});


export class AddUsers extends Component {
  static propTypes = {
    users: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      isLoading: true
    };
    this.saveUser = this.saveUser.bind(this);
  }

  componentDidMount() {
    // $('body').addClass('gray-bg');
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 3000);
  }
  componentWillUnmount() {
    // $('body').removeClass('gray-bg');
  }

  saveUser(values, form) {
    values.company_id = this.props.login.user.company_id;
    this.props.actions.addUser(values).then((data) => {
      notify.show(data.name + " has been added as a new client.", 'success');
      setTimeout(() => {
        this.props.history.push('/dashboard');
      }, 2000);
      console.info(data);
    }).catch((err) => {
      console.log("Error:", err);
    });
  }

  renderForm() {
    return (
      <Formik
        validationSchema={schema}
        onSubmit={this.saveUser}
        initialValues={{
          last_name: '',
          first_name: '',
          user_email: '',
          password: '',
          ce: false,
          pg: true,
          xml: false,
          epub: false,
          // phone: ''
        }}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          touched,
          isValid,
          errors,
        }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Row>
                <Form.Group as={Col} md="6" controlId="validationFormik02">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="first_name"
                    value={values.first_name}
                    onChange={handleChange}
                    isValid={touched.first_name && !errors.first_name}
                  />
                  <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} md="6" controlId="validationFormik02">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="last_name"
                    value={values.last_name}
                    onChange={handleChange}
                    isValid={touched.last_name && !errors.last_name}
                  />
                  <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="4" controlId="validationFormikUsername">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Users's Email"
                    aria-describedby="inputGroupPrepend"
                    name="user_email"
                    value={values.user_email}
                    onChange={handleChange}
                    isInvalid={!!errors.user_email}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.user_email}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} md="6" controlId="validationFormik05">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    name="password"
                    value={values.password}
                    onChange={handleChange}
                    isInvalid={!!errors.password}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.password}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Label>Products </Form.Label>
                <Form.Group as={Col} md="6" controlId="validationFormik03">
                  {this.props.login.user.Company.ce == 1 && (<Form.Check
                    inline
                    name="ce"
                    label="CE"
                    onChange={handleChange}
                    isInvalid={!!errors.ce}
                    feedback={errors.ce}
                    id="validationFormik0"
                    value="ce"
                  />)}
                  {this.props.login.user.Company.pg == 1 && (<Form.Check
                    inline
                    name="pg"
                    label="PG"
                    onChange={handleChange}
                    isInvalid={!!errors.pg}
                    feedback={errors.pg}
                    id="validationFormik0"
                    value="pg"
                  />)}
                  {this.props.login.user.Company.xml == 1 && (<Form.Check
                    inline
                    name="xml"
                    label="XML"
                    onChange={handleChange}
                    isInvalid={!!errors.xml}
                    feedback={errors.xml}
                    id="validationFormik0"
                    value="xml"
                  />)}
                  {this.props.login.user.Company.epub == 1 && (<Form.Check
                    inline
                    name="epub"
                    label="ePub"
                    onChange={handleChange}
                    isInvalid={!!errors.epub}
                    feedback={errors.epub}
                    id="validationFormik0"
                    value="epub"
                  />)}
                  {/* <Form.Control.Feedback type="invalid">
                    {errors.city}
                  </Form.Control.Feedback> */}
                </Form.Group>
              </Form.Row>
              {/* <Form.Row>
                <Form.Group as={Col} md="6" controlId="validationFormik05">
                  <Form.Label>Phone</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Phone"
                    name="phone"
                    value={values.phone}
                    onChange={handleChange}
                    isInvalid={!!errors.phone}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.phone}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row> */}

              <Button type="submit">Create</Button>
            </Form>
          )}
      </Formik>
    );
  }

  render() {
    return (
      <React.Fragment>
        {this.state.isLoading ? <Pace color="#27ae60" height={2} className="loader" /> : null}

        <div className="users-add-users">
          <Notifications />
          <Nav />
          <div id="page-wrapper" className="gray-bg">
            <div className="row border-bottom">
              <NavTop history={this.props.history} />
            </div>
            <div className="wrapper wrapper-content animated fadeInRight">

              <div className="row">
                
                <ConfigLeftCol />

                <div className="col-md-10">
                  <div className="ibox">
                    <div className="ibox-title">
                      <h5>Add Users</h5>
                      {/* <div className="ibox-tools">
                        <span className="label label-warning-light float-right">10 Messages</span>
                      </div> */}
                    </div>
                    <div className="ibox-content">
                      {this.renderForm()}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    users: state.users,
    login: state.login
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddUsers);
