// This is the JSON way to define React Router rules in a Rekit app.
// Learn more from: http://rekit.js.org/docs/routing.html

import {
  ListUsers,
  AddUsers,
} from './';

export default {
  path: 'users',
  name: 'Users',
  childRoutes: [
    { path: 'list-users', name: 'List users', component: ListUsers, isIndex: true },
    { path: 'new', name: 'Add users', component: AddUsers },
  ],
};
