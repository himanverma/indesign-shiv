import {
  DefaultPage,
} from './';

export default {
  path: '/HomeOld',
  name: 'Home',
  childRoutes: [
    { path: 'default-page',
      name: 'Default page',
      component: DefaultPage,
      isIndex: true,
    },
  ],
};
