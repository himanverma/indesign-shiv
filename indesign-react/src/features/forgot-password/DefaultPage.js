import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Link } from 'react-router-dom';

export class DefaultPage extends Component {
  static propTypes = {
    forgotPassword: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  componentDidMount() {
    document.body.classList.add("gray-bg");
  }
  componentWillUpdate() {
    document.body.classList.remove("gray-bg");
  }

  render() {
    return (
      <div className="forgot-password-default-page middle-box text-center loginscreen animated fadeInDown">
        <div>
          <div>
            <h1 className="logo-name">
              <img src="/logo.png" />

            </h1>
          </div>
          <h3>Welcome to PAS</h3>
          <p>Perfectly designed and precisely prepared for all your automated publication needs
      {/*Continually expanded and constantly improved Inspinia Admin Them (IN+)*/}
          </p>
          <p>Forgot Password</p>
          <form className="m-t" role="form" action="index.html">
            <div className="form-group">
              <input type="email" className="form-control" placeholder="Username" required />
            </div>
            <button type="submit" className="btn btn-primary block full-width m-b">Send Reset Link</button>
            <Link to="/login"><small>Login</small></Link>
            {/* <p className="text-muted text-center"><small>Do not have an account?</small></p> */}
            {/* <a className="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> */}
          </form>
          <p className="m-t"> <small>PAS group copyright © 2019</small> </p>
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    forgotPassword: state.forgotPassword,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultPage);
