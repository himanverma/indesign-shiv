import API from '../../../common/apiService';
import {
  LOGIN_LOGIN_NOW_BEGIN,
  LOGIN_LOGIN_NOW_SUCCESS,
  LOGIN_LOGIN_NOW_FAILURE,
  LOGIN_LOGIN_NOW_DISMISS_ERROR,
} from './constants';

// Rekit uses redux-thunk for async actions by default: https://github.com/gaearon/redux-thunk
// If you prefer redux-saga, you can use rekit-plugin-redux-saga: https://github.com/supnate/rekit-plugin-redux-saga
export function loginNow(args = {}) {
  return (dispatch) => { // optionally you can have getState as the second argument
    dispatch({
      type: LOGIN_LOGIN_NOW_BEGIN,
    });

    // Return a promise so that you could control UI flow without states in the store.
    // For example: after submit a form, you need to redirect the page to another when succeeds or show some errors message if fails.
    // It's hard to use state to manage it, but returning a promise allows you to easily achieve it.
    // e.g.: handleSubmit() { this.props.actions.submitForm(data).then(()=> {}).catch(() => {}); }
    const promise = new Promise((resolve, reject) => {
      // doRequest is a placeholder Promise. You should replace it with your own logic.
      // See the real-word example at:  https://github.com/supnate/rekit/blob/master/src/features/home/redux/fetchRedditReactjsList.js
      // args.error here is only for test coverage purpose.
      // const doRequest = args.error ? Promise.reject(new Error()) : Promise.resolve();
      const doRequest = API({
        url: 'login',
        method: 'POST',
        data: {
          "email": args.username,
          "password": args.password,
          "strategy": "local",
          // "deviceType": 3,
          // "deviceToken": "random",
          // "deviceId": "optional"
        }
      });
      doRequest.then(
        (res) => {
          dispatch({
            type: LOGIN_LOGIN_NOW_SUCCESS,
            data: res,
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        (err) => {
          dispatch({
            type: LOGIN_LOGIN_NOW_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

// Async action saves request error by default, this method is used to dismiss the error info.
// If you don't want errors to be saved in Redux store, just ignore this method.
export function dismissLoginNowError() {
  return {
    type: LOGIN_LOGIN_NOW_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case LOGIN_LOGIN_NOW_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        loginNowPending: true,
        loginNowError: null,
      };

    case LOGIN_LOGIN_NOW_SUCCESS:
      // The request is success
      localStorage.setItem('user', JSON.stringify(action.data.user));
      localStorage.setItem('accessToken', action.data.access_token.token);
      return {
        ...state,
        loginNowPending: false,
        loginNowError: null,
        accessToken: action.data.accessToken,
        user: action.data.user
      };

    case LOGIN_LOGIN_NOW_FAILURE:
      // The request is failed
      return {
        ...state,
        loginNowPending: false,
        loginNowError: action.data.error,
      };

    case LOGIN_LOGIN_NOW_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        loginNowError: null,
      };

    default:
      return state;
  }
}
