import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Link } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Notifications, { notify } from 'react-notify-toast';

export class DefaultPage extends Component {
  static propTypes = {
    login: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  componentDidMount() {
    document.body.classList.add("gray-bg");
  }
  componentWillUnmount() {
    document.body.classList.remove("gray-bg");
  }

  submitLoginForm(values, setSubmitting) {
    console.log(values);
    this.props.actions.loginNow(values).then((res) => {
      console.log(res);
      this.props.history.push('/dashboard');
    }).catch((er) => {
      setTimeout(() => {
        setSubmitting(false);
        notify.show(er.message, "error");
      }, 400);
    });

  }
  render() {
    return (
      <div className="login-default-page middle-box text-center loginscreen animated fadeInDown">
        <div>
          <div>
            <h1 className="logo-name">
              <img src="/logo.png" />

            </h1>
          </div>
          <h3>Welcome to PAS</h3>
          <p>Perfectly designed and precisely prepared for all your automated publication needs
      {/*Continually expanded and constantly improved Inspinia Admin Them (IN+)*/}
          </p>
          <p>Login in. To see it in action.</p>
          <Notifications />
          <Formik
            className="m-t"
            initialValues={{
              username: '',
              password: ''
            }}
            onSubmit={(values, { setSubmitting }) => { this.submitLoginForm(values, setSubmitting); }}
          >
            <Form>
              <div className="form-group">
                <Field name="username" type="email" className="form-control" placeholder="Username" required />
              </div>
              <div className="form-group">
                <Field name="password" type="password" className="form-control" placeholder="Password" required />
              </div>
              <button type="submit" className="btn btn-primary block full-width m-b">Login</button>
              <Link to="/forgot-password"><small>Forgot password?</small></Link>
              {/* <p className="text-muted text-center"><small>Do not have an account?</small></p> */}
              {/* <a className="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> */}
            </Form>
          </Formik>
          <p className="m-t"> <small>PAS group copyright © 2019</small> </p>
        </div>
      </div>

    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultPage);
