import API from '../../../common/apiService';
import {
  DOCUMENTS_LIST_AUTO_PAGING_BEGIN,
  DOCUMENTS_LIST_AUTO_PAGING_SUCCESS,
  DOCUMENTS_LIST_AUTO_PAGING_FAILURE,
  DOCUMENTS_LIST_AUTO_PAGING_DISMISS_ERROR,
} from './constants';

// Rekit uses redux-thunk for async actions by default: https://github.com/gaearon/redux-thunk
// If you prefer redux-saga, you can use rekit-plugin-redux-saga: https://github.com/supnate/rekit-plugin-redux-saga
export function listAutoPaging(args = {}) {
  return (dispatch) => { // optionally you can have getState as the second argument
    dispatch({
      type: DOCUMENTS_LIST_AUTO_PAGING_BEGIN,
    });

    // Return a promise so that you could control UI flow without states in the store.
    // For example: after submit a form, you need to redirect the page to another when succeeds or show some errors message if fails.
    // It's hard to use state to manage it, but returning a promise allows you to easily achieve it.
    // e.g.: handleSubmit() { this.props.actions.submitForm(data).then(()=> {}).catch(() => {}); }
    const promise = new Promise((resolve, reject) => {
      // doRequest is a placeholder Promise. You should replace it with your own logic.
      // See the real-word example at:  https://github.com/supnate/rekit/blob/master/src/features/home/redux/fetchRedditReactjsList.js
      // args.error here is only for test coverage purpose.
      // const doRequest = args.error ? Promise.reject(new Error()) : Promise.resolve();
      const doRequest = API({
        url: 'documents/auto-paging',
        method: 'GET',
        // data: args
      });
      doRequest.then(
        (res) => {
          dispatch({
            type: DOCUMENTS_LIST_AUTO_PAGING_SUCCESS,
            data: res,
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        (err) => {
          dispatch({
            type: DOCUMENTS_LIST_AUTO_PAGING_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

// Async action saves request error by default, this method is used to dismiss the error info.
// If you don't want errors to be saved in Redux store, just ignore this method.
export function dismissListAutoPagingError() {
  return {
    type: DOCUMENTS_LIST_AUTO_PAGING_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case DOCUMENTS_LIST_AUTO_PAGING_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        listAutoPagingPending: true,
        listAutoPagingError: null,
      };

    case DOCUMENTS_LIST_AUTO_PAGING_SUCCESS:
      // The request is success
      return {
        ...state,
        listAutoPagingPending: false,
        listAutoPagingError: null,
        autoPagingList: action.data.jobs
      };

    case DOCUMENTS_LIST_AUTO_PAGING_FAILURE:
      // The request is failed
      return {
        ...state,
        listAutoPagingPending: false,
        listAutoPagingError: action.data.error,
      };

    case DOCUMENTS_LIST_AUTO_PAGING_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        listAutoPagingError: null,
      };

    default:
      return state;
  }
}
