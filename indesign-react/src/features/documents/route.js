// This is the JSON way to define React Router rules in a Rekit app.
// Learn more from: http://rekit.js.org/docs/routing.html

import {
  AutoPaging,
} from './';

export default {
  path: 'documents',
  name: 'Documents',
  childRoutes: [
    { path: 'auto-paging', name: 'Auto paging', component: AutoPaging, isIndex: true },
  ],
};
