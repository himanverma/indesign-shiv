import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';

import Notifications, { notify } from 'react-notify-toast';
import { Formik } from 'formik'
import { Form, Button, Col, InputGroup } from 'react-bootstrap';
import Pace from 'react-pace-progress';
import Nav from '../common/Nav';
import NavTop from '../common/NavTop';

import $ from "jquery";
import * as yup from 'yup';

const API_URL = '//localhost:3030/api/v1/documents/auto-paging/download/';

const schema = yup.object({
  // document_file: yup.mixed().test('document_file',"Not"),
  template: yup.string().required(),
  // document_image: yup.string().required(),
});



export class AutoPaging extends Component {
  static propTypes = {
    documents: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      isLoading: true
    };
    this.uploadDocument = this.uploadDocument.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 3000);
    this.props.actions.listAutoPaging();
  }
  componentWillUnmount() {
  }
  
  uploadDocument(values){
    console.log(values);
    this.props.actions.uploadAutoPaging(values).then((dt)=>{
      this.props.actions.listAutoPaging();
    }).catch((err)=>{
      console.error(err);
    });
  }

  RenderUploadForm() {
    return (
      <Formik
        validationSchema={schema}
        onSubmit={this.uploadDocument}
        initialValues={{
          document_file: '',
          template: '',
          document_image: '',
        }}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          touched,
          isValid,
          errors,
          setFieldValue
        }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Row>
                <Form.Group as={Col} md="6" controlId="validationFormik02">
                  <Form.Label>Structure Document</Form.Label>
                  <Form.Control
                    type="file"
                    name="document_file"
                    // value={values.document_file}
                    onChange={(event) => {
                      setFieldValue("document_file", event.currentTarget.files[0]);
                    }}
                    isValid={touched.document_file && !errors.document_file}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.document_file}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>

                <Form.Group as={Col} md="6" controlId="validationFormik02">
                  <Form.Label>Select Template</Form.Label>
                  <Form.Control
                    as="select"
                    name="template"
                    value={values.template}
                    onChange={handleChange}
                    isValid={touched.template && !errors.template}
                  >
                    <option value="tmpl-1.indt">Template 1</option>
                    <option value="tmpl-2.indt">Template 2</option>
                    <option value="tmpl-3.indt">Template 3</option>
                    <option value="tmpl-4.indt">Template 4</option>
                    <option value="tmpl-5.indt">Template 5</option>
                  </Form.Control>
                  <Form.Control.Feedback type="invalid">
                    {errors.template}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="4" controlId="validationFormikUsername">
                  <Form.Label>Upload Image</Form.Label>
                  <Form.Control
                    type="file"
                    placeholder="Select/Drop"
                    aria-describedby="inputGroupPrepend"
                    name="document_image"
                    // value={values.document_image}
                    onChange={(event) => {
                      setFieldValue("document_image", event.currentTarget.files[0]);
                    }}
                    isInvalid={!!errors.document_image}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.document_image}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>

              <Button type="submit">Upload</Button>
            </Form>
          )}
      </Formik>
    );
  }
  ListSummary(){
    return (<table className="table table-bordered table-stripped">
      <thead>
        <tr>
          <th>S. No</th>
          <th>File Name</th>
          <th>Page Count</th>
          <th>Status</th>
          <th>Start Time</th>
          <th>End Time</th>
          <th>Duration</th>
          <th>Download</th>
        </tr>
      </thead>
      <tbody>
        {this.props.autoPagingList.map((doc,indx)=>{
          return (<tr>
            <td>{indx + 1}</td>
            <td>{doc.title}</td>
            <td>--</td>
            <td>{doc.status}</td>
            <td>{doc.started_at}</td>
            <td>{doc.ended_at}</td>
            <td>{doc.duration}</td>
            <td>
              <a href={API_URL + doc.title} target="_new">Download</a>
            </td>
          </tr>)
        })}
      </tbody>
    </table>)
  }
  render() {
    return (
      <React.Fragment>
        {this.state.isLoading ? <Pace color="#27ae60" height={2} className="loader" /> : null}

        <div className="documents-auto-paging">
          <Notifications />
          <Nav />
          <div id="page-wrapper" className="gray-bg">
            <div className="row border-bottom">
              <NavTop history={this.props.history} />
            </div>
            <div className="wrapper wrapper-content animated fadeInRight">

              <div className="row">
                <div className="col-md-12">
                  <div className="ibox">
                    <div className="ibox-title">
                      <h3>Auto Paging</h3>
                    </div>
                    <div className="ibox-content">
                      {this.RenderUploadForm()}
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="ibox">
                    <div className="ibox-title">
                      <h3>Summary</h3>
                    </div>
                    <div className="ibox-content">
                      {this.ListSummary()}
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    // documents: state.documents,
    autoPagingList: state.documents.autoPagingList
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AutoPaging);
