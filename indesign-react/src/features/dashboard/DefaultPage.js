import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import * as clientActions from '../clients/redux/actions';
import * as userActions from '../users/redux/actions';
import Pace from 'react-pace-progress';
import Nav from '../common/Nav';
import NavTop from '../common/NavTop';
import $ from "jquery";

import { Line } from 'react-chartjs-3';


export class DefaultPage extends Component {
  static propTypes = {
    dashboard: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  constructor() {
    super();
    this.state = {
      isLoading: true
    };
  }


  componentDidMount() {
    // $('body').addClass('gray-bg');
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 3000);
    if (this.props.login.user.if_client == 1) {
      this.props.actions.listUsers();
    }
    if (this.props.login.user.if_super_admin == 1) {
      this.props.actions.listClients();
    }
  }

  componentWillUnmount() {
    // $('body').removeClass('gray-bg');
  }

  ClientsList() {
    return (<div className="ibox ">
      <div className="ibox-title">
        <h5>Clients List </h5>
        <div className="ibox-tools">
          <a className="collapse-link">
            <i className="fa fa-chevron-up" />
          </a>
          {/* <a className="close-link">
            <i className="fa fa-times" />
          </a> */}
        </div>
      </div>
      <div className="ibox-content">
        <table className="table table-bordered">
          <thead>
            <tr>
              <th rowSpan={2}>ID</th>
              <th rowSpan={2}>Company Name</th>
              <th rowSpan={2}>Admin Name</th>
              <th rowSpan={2}>Admin Email</th>
              <th colSpan={4}>Products</th>
              <th rowSpan={2}>Status</th>
            </tr>
            <tr>
              <th>CE</th>
              <th>PG</th>
              <th>XML</th>
              <th>ePUB</th>
            </tr>
          </thead>
          <tbody>
            {this.props.clientsList.map((data, i) => {
              return <tr key={i}>
                <td>{data.id}</td>
                <td>{data.Company.company_name}</td>
                <td>{data.name}</td>
                <td>{data.email}</td>
                <td>{data.Company.ce ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.Company.pg ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.Company.xml ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.Company.epub ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>Active</td>
              </tr>
            })}
          </tbody>

        </table>
      </div>
    </div>
    )
  }

  UsersList() {
    return (<div className="ibox ">
      <div className="ibox-title">
        <h5>Users List </h5>
        <div className="ibox-tools">
          <a className="collapse-link">
            <i className="fa fa-chevron-up" />
          </a>
          {/* <a className="close-link">
            <i className="fa fa-times" />
          </a> */}
        </div>
      </div>
      <div className="ibox-content">
        <table className="table table-bordered">
          <thead>
            <tr>
              <th rowSpan={2}>ID</th>
              <th rowSpan={2}>Company Name</th>
              <th rowSpan={2}>Name</th>
              <th rowSpan={2}>Email</th>
              <th colSpan={4}>Products</th>
              <th rowSpan={2}>Status</th>
            </tr>
            <tr>
              <th>CE</th>
              <th>PG</th>
              <th>XML</th>
              <th>ePUB</th>
            </tr>
          </thead>
          <tbody>
            {this.props.usersList.map((data, i) => {
              return <tr key={i}>
                <td>{data.id}</td>
                <td>{data.Company.company_name}</td>
                <td>{data.name}</td>
                <td>{data.email}</td>
                <td>{data.ce ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.pg ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.xml ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>{data.epub ? (<i style={{ color: 'green' }} class="fa fa-check-square"></i>) : (<i style={{ color: 'red' }} class="fa fa-window-close"></i>)}</td>
                <td>Active</td>
              </tr>
            })}
          </tbody>

        </table>
      </div>
    </div>
    )
  }
  InfoBlocksSuper() {
    return (
      <div className="row">
        <div className="col-lg-2">
          <div className="ibox ">
            <div className="ibox-title">
              {/* <span className="label label-success float-right">Monthly</span> */}
              <h5>CopyEditing</h5>
            </div>
            <div className="ibox-content">
              <h1 className="no-margins">00</h1>
              <div className="stat-percent font-bold text-success">0% <i className="fa fa-bolt" /></div>
              <small>Total Jobs</small>
            </div>
          </div>
        </div>
        <div className="col-lg-2">
          <div className="ibox ">
            <div className="ibox-title">
              {/* <span className="label label-info float-right">Annual</span> */}
              <h5>Pagination</h5>
            </div>
            <div className="ibox-content">
              <h1 className="no-margins">00</h1>
              <div className="stat-percent font-bold text-info">0% <i className="fa fa-level-up" /></div>
              <small>Total Jobs</small>
            </div>
          </div>
        </div>

        <div className="col-lg-2">
          <div className="ibox ">
            <div className="ibox-title">
              {/* <span className="label label-success float-right">Monthly</span> */}
              <h5>XML</h5>
            </div>
            <div className="ibox-content">
              <h1 className="no-margins">00</h1>
              <div className="stat-percent font-bold text-success">0% <i className="fa fa-bolt" /></div>
              <small>Total Jobs</small>
            </div>
          </div>
        </div>
        <div className="col-lg-2">
          <div className="ibox ">
            <div className="ibox-title">
              {/* <span className="label label-info float-right">Annual</span> */}
              <h5>ePUB</h5>
            </div>
            <div className="ibox-content">
              <h1 className="no-margins">00</h1>
              <div className="stat-percent font-bold text-info">0% <i className="fa fa-level-up" /></div>
              <small>Total Jobs</small>
            </div>
          </div>
        </div>

        <div className="col-lg-2">
          <div className="ibox ">
            <div className="ibox-title">
              {/* <span className="label label-success float-right">Monthly</span> */}
              <h5>CLIENTS</h5>
            </div>
            <div className="ibox-content">
              <h1 className="no-margins">{this.props.clientsList.length}</h1>
              <div className="stat-percent font-bold text-success">100% <i className="fa fa-bolt" /></div>
              <small>Active</small>
            </div>
          </div>
        </div>
        <div className="col-lg-2">
          <div className="ibox ">
            <div className="ibox-title">
              {/* <span className="label label-info float-right">Annual</span> */}
              <h5>USERS</h5>
            </div>
            <div className="ibox-content">
              <h1 className="no-margins">{this.props.usersCount}</h1>
              <div className="stat-percent font-bold text-info">100% <i className="fa fa-level-up" /></div>
              <small>Active</small>
            </div>
          </div>
        </div>

      </div>
    )
  }
  InfoBlocksClient() {
    return (<div className="row">
      {this.props.login.user.Company.ce == 1 && (<div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-success float-right">Monthly</span> */}
            <h5>CopyEditing</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">00</h1>
            <div className="stat-percent font-bold text-success">0% <i className="fa fa-bolt" /></div>
            <small>Total Jobs</small>
          </div>
        </div>
      </div>)}
      {this.props.login.user.Company.pg == 1 && (<div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-info float-right">Annual</span> */}
            <h5>Pagination</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">00</h1>
            <div className="stat-percent font-bold text-info">0% <i className="fa fa-level-up" /></div>
            <small>Total Jobs</small>
          </div>
        </div>
      </div>)}

      {this.props.login.user.Company.xml == 1 && (<div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-success float-right">Monthly</span> */}
            <h5>XML</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">00</h1>
            <div className="stat-percent font-bold text-success">0% <i className="fa fa-bolt" /></div>
            <small>Total Jobs</small>
          </div>
        </div>
      </div>)}
      {this.props.login.user.Company.epub == 1 && (<div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-info float-right">Annual</span> */}
            <h5>ePUB</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">00</h1>
            <div className="stat-percent font-bold text-info">0% <i className="fa fa-level-up" /></div>
            <small>Total Jobs</small>
          </div>
        </div>
      </div>)}
      <div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-info float-right">Annual</span> */}
            <h5>USERS</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">{this.props.usersList.length}</h1>
            <div className="stat-percent font-bold text-info">100% <i className="fa fa-level-up" /></div>
            <small>Active</small>
          </div>
        </div>
      </div>
    </div>);
  }
  InfoBlocksUser() {
    return (<div className="row">
      {this.props.login.user.ce == 1 && (<div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-success float-right">Monthly</span> */}
            <h5>CopyEditing</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">00</h1>
            <div className="stat-percent font-bold text-success">0% <i className="fa fa-bolt" /></div>
            <small>Total Jobs</small>
          </div>
        </div>
      </div>)}
      {this.props.login.user.pg == 1 && (<div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-info float-right">Annual</span> */}
            <h5>Pagination</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">00</h1>
            <div className="stat-percent font-bold text-info">0% <i className="fa fa-level-up" /></div>
            <small>Total Jobs</small>
          </div>
        </div>
      </div>)}

      {this.props.login.user.xml == 1 && (<div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-success float-right">Monthly</span> */}
            <h5>XML</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">00</h1>
            <div className="stat-percent font-bold text-success">0% <i className="fa fa-bolt" /></div>
            <small>Total Jobs</small>
          </div>
        </div>
      </div>)}
      {this.props.login.user.epub == 1 && (<div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-info float-right">Annual</span> */}
            <h5>ePUB</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">00</h1>
            <div className="stat-percent font-bold text-info">0% <i className="fa fa-level-up" /></div>
            <small>Total Jobs</small>
          </div>
        </div>
      </div>)}
      <div className="col-lg-2">
        <div className="ibox ">
          <div className="ibox-title">
            {/* <span className="label label-info float-right">Annual</span> */}
            <h5>USERS</h5>
          </div>
          <div className="ibox-content">
            <h1 className="no-margins">{this.props.usersList.length}</h1>
            <div className="stat-percent font-bold text-info">100% <i className="fa fa-level-up" /></div>
            <small>Active</small>
          </div>
        </div>
      </div>
    </div>);
  }
  Chart() {
    let data = {
      labels: ['Apr', 'May', 'Jun', 'Jul'],
      height: 50,
      datasets: [{
        label: 'CE',
        backgroundColor: 'rgba(255,99,132,0.2)',
        data: [10, 20, 22, 28]
      }, {
        label: 'PG',
        backgroundColor: 'rgba(10,99,132,0.2)',
        data: [13, 18, 32, 25]
      }]
    }
    return (<div className="col-md-12">
      <div className="ibox">
        <div className="ibox-title">
          <h3>Usage Report</h3>
          <div className="pull-right">
            <select className="form-control">
              <option>Last 4 months</option>
              <option>Last 6 months</option>
            </select>
          </div>
        </div>
      </div>
      <Line data={data} height={100} />
    </div>);
  }
  render() {
    return (
      <React.Fragment>
        {this.state.isLoading ? <Pace color="#27ae60" height={2} className="loader" /> : null}

        <div className="dashboard-default-page" id="wrapper">
          <Nav />
          <div id="page-wrapper" className="gray-bg">
            <div className="row border-bottom">
              <NavTop history={this.props.history} />
            </div>
            {/* <div className="row wrapper border-bottom white-bg page-heading">
              <div className="col-sm-4">
                <h2>This is main title</h2>
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">
                    <a href="index.html">This is</a>
                  </li>
                  <li className="breadcrumb-item active">
                    <strong>Breadcrumb</strong>
                  </li>
                </ol>
              </div>
              <div className="col-sm-8">
                <div className="title-action">
                  <a href className="btn btn-primary">This is action area</a>
                </div>
              </div>
            </div> */}
            <div className="wrapper wrapper-content">
              {this.props.login.user.if_super_admin == 1 && (this.InfoBlocksSuper())}
              {this.props.login.user.if_client == 1 && (this.InfoBlocksClient())}
              {this.props.login.user.if_user == 1 && (this.InfoBlocksUser())}
              <div className="row">
                {this.Chart()}
              </div>
              <div className="row">
                <div className="col-md-12">
                  {this.props.login.user.if_super_admin == 1 && (this.ClientsList())}
                  {this.props.login.user.if_client == 1 && (this.UsersList())}
                </div>
              </div>

            </div>


            <div className="footer">
              <div className="float-right">
                10GB of <strong>250GB</strong> Free.
          </div>
              <div>
                <strong>Copyright</strong> Example Company © 2014-2018
          </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    dashboard: state.dashboard,
    clientsList: state.clients.clientsList,
    usersCount: state.clients.usersCount,
    login: state.login,
    usersList: state.users.usersList
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions, ...clientActions, ...userActions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultPage);
