import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Link } from 'react-router-dom';

export class ConfigLeftCol extends Component {
  static propTypes = {
    common: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div className="common-config-left-col col-md-2">
        <div className="row">
          <h3>Configuration</h3>
        </div>
        <table className="table table-hover margin bottom">
          {this.props.login.user.if_super_admin == 1 && (
            <tbody>
              {/* <tr>
                <td>
                  <Link to="/users/new">
                    <i className="fa fa-user-plus" aria-hidden="true"></i> Add Users
                  </Link>
                </td>
              </tr> */}
              <tr>
                <td>
                  <Link to="/users">
                    <i className="fa fa-user" aria-hidden="true"></i> Manage Users
                  </Link>
                </td>
              </tr>
              <tr>
                <td>
                  <Link to="/clients/new">
                    <i className="fa fa-plus" aria-hidden="true"></i> Add Clients
                          </Link>
                </td>
              </tr>
              <tr>
                <td>
                  <Link to="/clients">
                    <i className="fa fa-edit" aria-hidden="true"></i> Manage Clients
                          </Link>
                </td>
              </tr>
            </tbody>)}
          {this.props.login.user.if_client == 1 && (
            <tbody>
              <tr>
                <td>
                  <Link to="/users/new">
                    <i className="fa fa-user-plus" aria-hidden="true"></i> Add Users
                  </Link>
                </td>
              </tr>
              <tr>
                <td>
                  <Link to="/users">
                    <i className="fa fa-user" aria-hidden="true"></i> Manage Users
                  </Link>
                </td>
              </tr>
            </tbody>)}
        </table>


      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    common: state.common,
    login: state.login
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfigLeftCol);
