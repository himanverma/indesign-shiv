import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import jQuery from 'jquery';

import { Link } from 'react-router-dom'
// import slimscroll  'jquery-slimscroll';
// import jquery from 'jquery';
import metismenu from 'metismenu';
import bootstrap from 'bootstrap';

export class Nav extends Component {
  static propTypes = {
    common: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  fix_height() {
    var heightWithoutNavbar = jQuery(".page-container > #wrapper").height() - 62;
    jQuery(".sidebar-panel").css("min-height", heightWithoutNavbar + "px");

    var navbarheight = jQuery('nav.navbar-default').height();
    var wrapperHeight = jQuery('#page-wrapper').height();

    if (navbarheight > wrapperHeight) {
      jQuery('#page-wrapper').css("min-height", navbarheight + "px");
    }

    if (navbarheight < wrapperHeight) {
      jQuery('#page-wrapper').css("min-height", jQuery(window).height() + "px");
    }

    if (jQuery('body').hasClass('fixed-nav')) {
      if (navbarheight > wrapperHeight) {
        jQuery('#page-wrapper').css("min-height", navbarheight + "px");
      } else {
        jQuery('#page-wrapper').css("min-height", jQuery(window).height() - 60 + "px");
      }
    }

  }

  SmoothlyMenu() {
    if (!jQuery('body').hasClass('mini-navbar') || jQuery('body').hasClass('body-small')) {
      // Hide menu in order to smoothly turn on when maximize menu
      jQuery('#side-menu').hide();
      // For smoothly turn on menu
      setTimeout(
        function () {
          jQuery('#side-menu').fadeIn(400);
        }, 200);
    } else if (jQuery('body').hasClass('fixed-sidebar')) {
      jQuery('#side-menu').hide();
      setTimeout(
        function () {
          jQuery('#side-menu').fadeIn(400);
        }, 100);
    } else {
      // Remove all inline style from jquery fadeIn function to reset menu state
      jQuery('#side-menu').removeAttr('style');
    }
  }

  componentDidMount() {
    var _this = this;
    // Fixed Sidebar
    // if (jQuery("body").hasClass('fixed-sidebar')) {
    //   jQuery('.sidebar-collapse').slimScroll({
    //     height: '100%',
    //     railOpacity: 0.9
    //   });
    // }

    // MetisMenu
    var sideMenu = jQuery('#side-menu').metisMenu();

    // sideMenu.on('shown.metisMenu', (e) => {
    //   this.fix_height();
    //   console.log(sideMenu);
    // });

    // Minimalize menu
    jQuery('.navbar-minimalize').on('click', (event) => {
      event.preventDefault();
      jQuery("body").toggleClass("mini-navbar");
      _this.SmoothlyMenu();
    });
    jQuery('.tooltip-demo').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
    });

    jQuery("[data-toggle=popover]")
      .popover();

    // jQuery('.full-height-scroll').slimscroll({
    //   height: '100%'
    // })

  }

  render() {
    return (
      <nav className="common-nav navbar-default navbar-static-side" role="navigation">
        <div className="sidebar-collapse">
          <ul className="nav metismenu" id="side-menu">
            <li className="nav-header">
              <div className="dropdown profile-element">
                <img alt="image" className="rounded-circle" src="/img/profile_small.jpg" />
                <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                  <span className="block m-t-xs font-bold">{ this.props.login.user.name }</span>
                  <span className="text-muted text-xs block">
                    {this.props.login.user.if_super_admin == 1 && "Super Admin" }
                    {this.props.login.user.if_client == 1 && this.props.login.user.Company.company_name }
                    {this.props.login.user.if_user == 1 && this.props.login.user.Company.company_name }
                    <b className="caret" /></span>
                </a>
                <ul className="dropdown-menu animated fadeInRight m-t-xs">
                  <li><a className="dropdown-item" href="#">Edit Profile</a></li>
                  {/* <li><a className="dropdown-item" href="contacts.html">Contacts</a></li>
                  <li><a className="dropdown-item" href="mailbox.html">Mailbox</a></li> */}
                  {/* <li className="dropdown-divider" /> */}
                  {/* <li><Link className="dropdown-item">Logout</Link></li> */}
                </ul>
              </div>
              <div className="logo-element">
                PAS
              </div>
            </li>
            <li>
              <Link to="/dashboard"><i className="fa fa-diamond" /> <span className="nav-label">Dashboard</span></Link>
            </li>


            {this.props.login.user.if_super_admin == 1 && (
              <li><Link to="/clients/new"><i className="fa fa-diamond" /> <span className="nav-label">Configuration</span></Link></li>
            )}
            {this.props.login.user.if_client == 1 && (
              <li><Link to="/users/new"><i className="fa fa-diamond" /> <span className="nav-label">Configuration</span></Link></li>
            )}
            {this.props.login.user.if_user == 1 && (<React.Fragment>
              <li><Link to="/documents/auto-paging"><i className="fa fa-diamond" /> <span className="nav-label">Auto Paging</span></Link></li>
              </React.Fragment>)}
            {/* {this.props.login.user.if_user == 1 && (
                <li><Link to="/documents/upload/pg"><i className="fa fa-diamond" /> <span className="nav-label">New Auto Pagination</span></Link></li>
              )} */}
            {/* <li>
              <a href="#"><i className="fa fa-th-large" /> <span className="nav-label">Configuration</span> <span className="fa arrow" /></a>
              <ul className="nav nav-second-level collapse">
                <li><a href="#">Admin</a></li>
                <li><a href="#">User</a></li>
              </ul>
            </li> */}
            <li>
              <a href="#"><i className="fa fa-bar-chart-o" /> <span className="nav-label">Features</span><span className="fa arrow" /></a>
              <ul className="nav nav-second-level collapse">
                <li><a href="#">CopyEditing</a></li>
                <li><a href="#">Pagination</a></li>
                <li><a href="#">XML</a></li>
                <li><a href="#">ePub</a></li>
              </ul>
            </li>

          </ul>
        </div>
      </nav>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    common: state.common,
    login: state.login
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav);
