import { App } from '../features/home';
import { PageNotFound } from '../features/common';
import commonRoute from '../features/common/route';
import examplesRoute from '../features/examples/route';
import _ from 'lodash';
import loginRoute from '../features/login/route';
import dashboardRoute from '../features/dashboard/route';
import usersRoute from '../features/users/route';
import forgotPasswordRoute from '../features/forgot-password/route';
import landingRoute from '../features/landing/route';
import clientsRoute from '../features/clients/route';
import documentsRoute from '../features/documents/route';

// NOTE: DO NOT CHANGE the 'childRoutes' name and the declaration pattern.
// This is used for Rekit cmds to register routes config for new features, and remove config when remove features, etc.
const childRoutes = [
  commonRoute,
  examplesRoute,
  loginRoute,
  dashboardRoute,
  usersRoute,
  forgotPasswordRoute,
  landingRoute,
  clientsRoute,
  documentsRoute,
];

const routes = [{
  path: '/',
  component: App,
  childRoutes: [
    ...childRoutes,
    { path: '*', name: 'Page not found', component: PageNotFound },
  ].filter(r => r.component || (r.childRoutes && r.childRoutes.length > 0)),
}];

// Handle isIndex property of route config:
//  Dupicate it and put it as the first route rule.
function handleIndexRoute(route) {
  if (!route.childRoutes || !route.childRoutes.length) {
    return;
  }

  const indexRoute = _.find(route.childRoutes, (child => child.isIndex));
  if (indexRoute) {
    const first = { ...indexRoute };
    first.path = '';
    first.exact = true;
    first.autoIndexRoute = true; // mark it so that the simple nav won't show it.
    route.childRoutes.unshift(first);
  }
  route.childRoutes.forEach(handleIndexRoute);
}

routes.forEach(handleIndexRoute);
export default routes;
