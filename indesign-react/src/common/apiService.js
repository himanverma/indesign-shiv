import axios from 'axios';
import {notify} from 'react-notify-toast';
const client = axios.create({
    // baseURL: `http://192.168.0.107:8009/api/v1`, // proccess.env.
    baseURL: `http://localhost:3030/api/v1/`,
    timeout: 5000,
    // withCredentials: true,
    // transformRequest: [(data) => JSON.stringify(data.data)],
    headers: {
        'Accept': 'application/json',
        // 'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    },
});

const request = function (options) {
    if(localStorage.accessToken){
        console.log('auth');
        axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.accessToken;
    }
    const onSuccess = function (response) {
        console.debug('Request Successful!', response);
        return response.data;
    }

    const onError = function (error) {
        // alert(123123);
        console.warn('Request Failed:', error.response);
        if (error.response) {
            // Request was made but server responded with something
            // other than 2xx
            if(error.response.status == 401){
                notify.show(error.response.data.message,"error");
                if(window.location.pathname != "/login"){
                    setTimeout(()=>{
                        localStorage.clear();
                        window.location = "/login";
                    },1000)
                }
                return Promise.reject(error.response || error.message);
            }
            notify.show(error.response.data.error.message,"error");
            return Promise.reject(error.response || error.message);

            
            if(error.response.status == 422 || error.response.status == 412){
                notify.show(error.response.data.error.message,"error");
            }
            if(error.response.status == 403){ 
                notify.show(error.response.data.error.message,"error");
            }
            

            // console.error('Status:', error.response.status);
            // console.error('Data:', error.response.data);
            // console.error('Headers:', error.response.headers);

        } else {
            // Something else happened while setting up the request
            // triggered the error
            notify.show("API Server not working or Check your Internet connetion.","error");
            console.error('Error Message:', error.message);
        }

        return Promise.reject(error.response || error.message);
    }

    return client(options)
        .then(onSuccess)
        .catch(onError);
};
export default request;