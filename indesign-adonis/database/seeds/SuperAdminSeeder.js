'use strict'

/*
|--------------------------------------------------------------------------
| SuperAdminSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')
const Hash = use('Hash')

class SuperAdminSeeder {
  async run () {
    const SuperAdmin = await Database.table('users').insert({
      name: 'SuperAdmin',
      first_name: 'Shiv',
      last_name: 'Ram',
      email: 'sadmin@yopmail.com',
      password: await Hash.make('password123'),
      if_super_admin: true,
      if_client: false,
      if_user: false,
    });
  }
}

module.exports = SuperAdminSeeder
