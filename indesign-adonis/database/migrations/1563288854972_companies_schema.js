'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompaniesSchema extends Schema {
  up () {
    this.create('companies', (table) => {
      table.increments()

      table.string('company_name')
      table.integer('ce')
      table.integer('pg')
      table.integer('epub')
      table.integer('xml')
      table.string('country')
      table.integer('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('companies')
  }
}

module.exports = CompaniesSchema
