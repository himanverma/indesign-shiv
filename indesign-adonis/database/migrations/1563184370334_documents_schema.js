'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DocumentsSchema extends Schema {
  up () {
    this.create('documents', (table) => {
      table.increments()
      
      table.string('title')
      table.string('jobType')
      table.string('path')
      table.integer('processed')
      table.integer('status')
      table.string('started_at')
      table.string('ended_at')
      table.string('duration')
      table.integer('company_id')
      table.integer('user_id')
      table.timestamps()
     

    })
  }

  down () {
    this.drop('documents')
  }
}

module.exports = DocumentsSchema
