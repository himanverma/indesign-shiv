'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      // table.string('username', 80).notNullable().unique()
      table.string('name');
      table.string('first_name');
      table.string('last_name');
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.integer('if_super_admin')
      table.integer('if_client')
      table.integer('if_user')
      table.integer('company_id')

      table.integer('ce')
      table.integer('pg')
      table.integer('epub')
      table.integer('xml')
      table.integer('status')
        // table.foreign('company_id').references('companies.id').onDelete('NULL')
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
