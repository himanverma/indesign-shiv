#!/bin/sh

cd /app  
npm i -g adonis
adonis migration:refresh
adonis seed
npm start