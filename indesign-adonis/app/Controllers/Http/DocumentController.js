'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @typedef {import('@adonisjs/framework/src/Helpers')} Helpers */

const Company = use('App/Models/Company');
const Document = use('App/Models/Document');
const Helpers = use('Helpers');
const Drive = use('Drive');
const fs = require('fs');
const archiver = require('archiver');
 

const makeid = function (length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}


const zipDirectory = function (source, out) {
  const archive = archiver('zip', { zlib: { level: 9 }});
  const stream = fs.createWriteStream(out);

  return new Promise((resolve, reject) => {
    archive
      .directory(source, false)
      .on('error', err => reject(err))
      .pipe(stream)
    ;

    stream.on('close', () => resolve());
    archive.finalize();
  });
}
/**
 * Resourceful controller for interacting with documents
 */
class DocumentController {

  async downloadAutoPaging({ request, response, params }) {
    let document = await Document.query().where('title', params.title).fetch();
    if (document.length == 0) {
      return response.json({
        error: "Invalid Job"
      })
    }
    var outPath = Helpers.tmpPath(document.first().title + '.zip');
    await zipDirectory(document.first().path + '.',outPath).then((r)=>{
      return response.download(outPath)
    });
  }

  async listAutoPaging({ request, response, view, auth }) {
    const authUser = await auth.getUser();
    let document = await Document.query().where('company_id', authUser.company_id).fetch();
    return response.json({
      jobs: document
    });
  }
  async createAutoPaging({ request, response, view, auth }) {
    const authUser = await auth.getUser();
    // const comp = await Company.findBy('id',authUser.company_id);
    const compId = authUser.company_id;
    const jobId = makeid(8);
    const DocFile = request.file('document_file');
    const ImgFile = request.file('document_image');
    const ClientPath = Helpers.publicPath(`uploads/IN/CLI-${compId}/${jobId}/`);
    const TmplPath = Helpers.publicPath(`uploads/TMPL/`);
    const selectedTmpl = request.input('template', 'tmpl-1.indt');

    let job = new Document();
    job.jobType = 'pg';
    job.company_id = authUser.company_id;
    job.user_id = authUser.id;
    job.title = jobId;
    job.path = ClientPath;
    job.processed = 0;
    job.status = 1;
    job.started_at = null;
    job.ended_at = null;
    job.duration = null;
    let jbResult = await job.save();

    await DocFile.move(ClientPath + 'IN', {
      name: makeid(7) + ".docx"
    });
    await ImgFile.move(ClientPath + 'IN', {
      name: makeid(7) + ".docx"
    });
    await Drive.copy(TmplPath + selectedTmpl, ClientPath + 'TMPL/' + selectedTmpl);
    await Drive.put(ClientPath + 'OUT/.empty', "");
    let tmlCode = '<?xml version="1.0" encoding="UTF-8"?>';
    tmlCode += `<pas><jobId>${jobId}</jobId><template>${selectedTmpl}</template></pas>`;
    await Drive.put(ClientPath + '/job.xml', tmlCode);
    // console.log('DocFile',DocFile);
    return response.json({
      job: job
    });
    // documents/auto-paging/upload
  }


  /**
   * Show a list of all documents.
   * GET documents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new document.
   * GET documents/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new document.
   * POST documents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single document.
   * GET documents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing document.
   * GET documents/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update document details.
   * PUT or PATCH documents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a document with id.
   * DELETE documents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = DocumentController
