'use strict'

const User = use('App/Models/User');

class AuthController {

    async register({request, auth, response}) {
        

        const {username,email,password,if_super_admin,if_client,if_user}=request.post()
        const users=await User.create({username,email,password,if_super_admin,if_client,if_user})

        let accessToken = await auth.generate(users)
        return response.json({"user": users, "access_token": accessToken})
    }
    
    async login({request, auth, response}) {
        const {email,password}=request.post()
        console.log(email,password)
        try {
          if (await auth.attempt(email, password)) {
            let user = await User.findBy('email', email)
            let accessToken = await auth.generate(user)
            if(user.company_id != null){
              await user.load('Company');
            }
            return response.json({"user":user, "access_token": accessToken})
          }

        }
        catch (e) {
          console.log(e);
          return response.unauthorized({message: 'You first need to register!'});
          // return response.json({message: 'You first need to register!'})
        }
    }

    async index({request, auth, response}) {
        
        const users=await User.all();
        return response.json({"user": users})
    }


}

module.exports = AuthController


