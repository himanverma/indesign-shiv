'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Client = use('App/Models/Client')
const User = use('App/Models/User')
const Company = use('App/Models/Company')


/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')


/**
 * Resourceful controller for interacting with clients
 */
class ClientController {
  /**
   * Show a list of all clients.
   * GET clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {

    let clients = await User.query().with('Company').where('if_client', 1).fetch();
    if (!clients) {
      clients = [];
    }
    // for(i of clients){
    //   clients.Company().fetch();
    // }
    response.json({
      message: 'Here the list of clients',
      clients: clients,
      usersCount: clients.length
    });
  }

  async users({request,response, auth}){
    const authUser = await auth.getUser();
    
    let users = await User.query().with('Company').where({
      if_user:1,
      company_id: authUser.company_id 
    }).fetch()

    return response.json({
      users: users
    });
  }

  async userCreate({request,response, auth}){
    const authUser = await auth.getUser();
    const comp = await Company.findBy('id',authUser.company_id);

    // console.log(comp);
    // return false;
    let data = request.post();
    let name =  data.first_name + " " + data.last_name
    console.log(data);
    let user = new User()
    user.first_name = data.first_name;
    user.last_name = data.last_name;
    user.name = name.trim();
    user.email = data.user_email;
    user.password = data.password;
    user.if_super_admin = 0;
    user.if_client = 0;
    user.if_user = 1;
    user.company_id = authUser.company_id;
    if(comp.ce){
      user.ce = data.ce || 0
    }
    if(comp.pg){
      user.pg = data.pg || 0
    }
    if(comp.xml){
      user.xml = data.xml || 0
    }
    if(comp.epub){
      user.epub = data.epub || 0
    }
    // user.phone = data.phone;
    return response.json({
      message: "Users created successfully.",
      user: await user.save() 
    });
    
  }

  /**
   * Render a form to be used for creating a new client.
   * GET clients/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view, auth }) {


    const data = request.post()

    const company = {}
    company.company_name = data.company_name
    company.ce = data.ce
    company.pg = data.pg
    company.epub = data.epub
    company.country = data.country
    company.xml = data.xml

    const companycreate = await Company.create(company);

    const user = {}
    user.name = data.admin_name
    user.email = data.admin_email
    user.password = data.password
    user.if_super_admin = false
    user.if_client = true
    user.if_user = false
    user.company_id = companycreate.id

    const usercreate = await User.create(user);




    // let accessToken = await auth.generate(usercreate)

    response.json({
      message: "Saved Successfully",
      data: {
        'user': user,
        'company': company,
        // 'accesstoken':accessToken
      }
    })

  }

  async login({ auth, request, response }) {
    const { email, password } = request.post()
    //const authchek=await auth.attempt(email, password)
    // console.log(authchek)
    // return response.json({message: email})
    try {

      if (await auth.attempt(email, password)) {
        let user = await User.findBy('email', email)
        let accessToken = await auth.generate(user)
        return response.json({ "user": user, "access_token": accessToken })
      }

    }
    catch (e) {
      return response.json({ message: 'You first need to register!' })
    }
  }

  /**
   * Create/save a new client.
   * POST clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single client.
   * GET clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing client.
   * GET clients/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update client details.
   * PUT or PATCH clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a client with id.
   * DELETE clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }



}

module.exports = ClientController
