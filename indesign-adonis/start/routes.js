'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
//const ClientController=use('App/Controllers/Http/ClientController');

Route.on('/').render('welcome')
Route.group(() => {
   Route.post('register', 'AuthController.register')
   Route.post('login', 'AuthController.login')
   Route.get('users','AuthController.index').middleware('auth');
   Route.get('company','CompanyController.index').middleware('auth');
   Route.post('clients','ClientController.create').middleware('auth');
   Route.get('clients','ClientController.index').middleware('auth');
   Route.get('clients/users','ClientController.users').middleware('auth');
   Route.post('clients/users','ClientController.userCreate').middleware('auth');

   Route.get('documents/auto-paging','DocumentController.listAutoPaging').middleware('auth');
   Route.post('documents/auto-paging/upload','DocumentController.createAutoPaging').middleware('auth');
   Route.get('documents/auto-paging/download/:title','DocumentController.downloadAutoPaging');
    
  }).prefix('api/v1')
