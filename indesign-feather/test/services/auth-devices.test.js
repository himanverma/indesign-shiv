const app = require('../../src/app');

describe('\'AuthDevices\' service', () => {
  it('registered the service', () => {
    const service = app.service('auth-devices');
    expect(service).toBeTruthy();
  });
});
