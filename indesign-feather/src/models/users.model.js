// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;
// const Company = require('./companies.model');
// console.log(Company());

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const users = sequelizeClient.define('users', {
    // company_id: {
    //   type: DataTypes.INTEGER,
    //   allowNull: true,
    //   defaultValue: null,
    //   references: {
    //     model: Company.companies,
    //     key: 'id'
    //   }
    // },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    if_super_admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    if_client: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    if_user: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }


  }, {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      }
    });

  // eslint-disable-next-line no-unused-vars
  users.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    users.belongsTo(models.companies, {
      as: 'Company',
      foreignKey: 'company_id'
    });
  };

  return users;
};
