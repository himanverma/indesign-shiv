// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const companies = sequelizeClient.define('companies', {
    company_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    ce: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    pg: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    epub: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    xml: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    country: {
      type: DataTypes.STRING,
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  companies.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return companies;
};
