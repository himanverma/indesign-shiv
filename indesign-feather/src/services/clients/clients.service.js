// Initializes the `Clients` service on path `/clients`
const createService = require('./clients.class.js');
const hooks = require('./clients.hooks');
const usersModel = require('../../models/users.model');
const companyModel = require('../../models/companies.model');

module.exports = function (app) {
  const CompanyModel = companyModel(app);
  const UserModel =  usersModel(app);
  const paginate = app.get('paginate');
  const options = {
    CompanyModel,
    UserModel,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/clients', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('clients');

  service.hooks(hooks);
};
