/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
    // this.options.UserModel.findAll({
    //   where: {
    //     if_client:true
    //   },
    //   include:[
    //     {
    //       model: this.options.CompanyModel,
    //       as: 'Company'
    //     }
    //   ]
    // }).then((data)=>{
    //   console.log(data[0]);
    //   // console.log(data[0],data[0].getCompany().then((c) => {console.log(c)}));
    //   // result = data;
    // }).catch((err)=>{
    //   console.log(err);
    //   // result = err;
    // });
  }

  async find(params) {
    var result = {
      clients: [],
      usersCount: 0
    };

    // http://docs.sequelizejs.com/manual/querying.html
    await this.options.UserModel.findAll({
      where: {
        if_client: true
      },
      include: [
        {
          model: this.options.CompanyModel,
          as: 'Company',
          // where: { state: Sequelize.col('project.state') }
        }
      ],
      // offset: 5, 
      // limit: 5,
      order: [
        ['createdAt', 'DESC'],
      ]
    }).then((data) => {
      // console.log(data[0]);
      result.clients = data;
    }).catch((err) => {
      console.log(err);
      result = err;
    });
    await this.options.UserModel.findAll({
      where: {
        if_user: true
      }
    }).then((d) => {
      result.usersCount = d.length;
    });
    return result;
  }

  async get(id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create(data, params) {
    var response = {
      company: null,
      admin: null
    };
    await this.options.CompanyModel.create({
      company_name: data.company_name,
      ce: data.ce,
      pg: data.pg,
      epub: data.epub,
      xml: data.xml,
      country: data.country
    }).then((company) => {
      response.company = company;
    }).catch((err) => {
      console.log(err);
      return err;
    });
    await this.options.UserModel.create({
      company_id: response.company.id,
      name: data.admin_name,
      email: data.admin_email,
      password: data.password,
      if_super_admin: false,
      if_client: true,
      if_user: false
    }).then((user) => {
      response.admin = user;
      return user;
    }).catch((err) => {
      console.log(err);
      return err;
    });
    // if (Array.isArray(data)) {
    //   return Promise.all(data.map(current => this.create(current, params)));
    // }

    return response;
  }

  async update(id, data, params) {
    return data;
  }

  async patch(id, data, params) {
    return data;
  }

  async remove(id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
