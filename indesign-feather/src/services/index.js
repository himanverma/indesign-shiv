const users = require('./users/users.service.js');
const companies = require('./companies/companies.service.js');
const auth = require('./auth/auth.service.js');
const clients = require('./clients/clients.service.js');
const clientUsers = require('./client-users/client-users.service.js');
const documents = require('./documents/documents.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(companies);
  app.configure(users);
  app.configure(auth);
  app.configure(clients);
  app.configure(clientUsers);
  app.configure(documents);
};
