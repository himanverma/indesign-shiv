/* eslint-disable no-console */
/* eslint-disable no-unused-vars */

class Service {
  constructor (options) {
    this.options = options || {};
    const sequelizeClient = options.app.get('sequelizeClient');
    console.log(sequelizeClient);
  }

  async find (params) {
    console.log(params);
    var result = {
      users: [],
      usersCount: 0
    };
    await this.options.UserModel.findAll({
      where: {
        if_user: true
      },
      include: [
        {
          model: this.options.CompanyModel,
          as: 'Company',
          // where: { state: Sequelize.col('project.state') }
        }
      ],
      // offset: 5, 
      // limit: 5,
      order: [
        ['createdAt', 'DESC'],
      ]
    }).then((data) => {
      // console.log(data[0]);
      result.users = data;
    }).catch((err) => {
      console.log(err);
      result = err;
    });
    await this.options.UserModel.findAll({
      where: {
        if_user: true
      }
    }).then((d) => {
      result.usersCount = d.length;
    });
    return result;
  }

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    console.log(data,params);
    var response = {
      user: null
    };
    await this.options.UserModel.create({
      company_id: data.company_id,
      name: data.user_name,
      email: data.user_email,
      password: data.password,
      if_super_admin: false,
      if_client: false,
      if_user: true
    }).then((user) => {
      response.user = user;
      return user;
    }).catch((err) => {
      console.log(err);
      return err;
    });

    return data;
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
