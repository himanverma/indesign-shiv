// Initializes the `ClientUsers` service on path `/client-users`
const createService = require('./client-users.class.js');
const hooks = require('./client-users.hooks');
// const companyModel = require('../../models/companies.model');
// const usersModel = require('../../models/users.model');



module.exports = function (app) {
  // const CompanyModel = companyModel(app);
  // const UserModel =  usersModel(app);
  const paginate = app.get('paginate');

  const options = {
    app,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/client-users', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('client-users');

  service.hooks(hooks);
};
